#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include "boost/nondet_random.hpp"
#include "ceelo.h"
#include "term.h"

constexpr auto diceCount = 3;
constexpr auto startBalance = 500;

std::vector<int> playerDice(diceCount);
std::vector<int> opponentDice(diceCount);
auto playerBalance = startBalance;
auto opponentBalance = startBalance;
auto bet = 0;

int main(int argc, char* argv[]) {
	// std::random_device doesn't work on MinGW
	boost::random::random_device rng;
	std::mt19937 urng(rng());

	while(playerBalance > 0 && opponentBalance > 0) {
		term::clear();

		std::cout << "Your balance:       " << playerBalance << std::endl;
		std::cout << "Opponent's balance: " << opponentBalance << '\n' << std::endl;

		prompt_bet();
		std::cout << std::endl;

		do {
			roll_dice(playerDice, urng);
		} while(roll_type(playerDice) == Roll::none);
		do{
			roll_dice(opponentDice, urng);
		}while(roll_type(opponentDice) == Roll::none);

		std::cout << "Your roll:     ";
		print_dice(playerDice);
		std::cout << std::endl << "Opponent roll: ";
		print_dice(opponentDice);
		std::cout << '\n' <<  std::endl;

		compare_rolls();
		std::cout << std::endl;
		term::pause();
	}
}

void prompt_bet() {
	do {
		std::cout << "Enter bet: ";
		std::cin >> bet;
	} while(!(bet > 0 && bet <= playerBalance && bet <= opponentBalance));

	playerBalance -= bet;
	opponentBalance -= bet;
}

void roll_dice(std::vector<int>& dice, std::mt19937& urng) {
	std::uniform_int_distribution<> roll(1, 6);
	for(auto& die : dice) {
		die = roll(urng);
	}

	std::sort(dice.begin(), dice.end());
}

void print_dice(const std::vector<int>& dice) {
	for(auto die : dice) {
		std::cout << die << ' ' << std::flush;
	}
}

Roll roll_type(const std::vector<int>& dice) {
	const std::vector<int> win {4, 5, 6};
	const std::vector<int> lose {1, 2, 3};

	if(dice == win) return Roll::win;
	if(dice == lose) return Roll::lose;

	auto sameCount = 0;

	for(auto i = 1; i < dice.size(); ++i) {
		if(dice.at(i) == dice.at(i - 1)) {
			++sameCount;
		}
	}

	switch(sameCount) {
		case(1): return Roll::pair;
		case(2): return Roll::trip;
		default: return Roll::none;
	}
}

void compare_rolls() {
	auto opponentRoll = roll_type(opponentDice);
	auto playerRoll = roll_type(playerDice);

	if(opponentRoll == playerRoll) {
		if(opponentRoll == Roll::trip) {
			if(playerDice.at(0) > opponentDice.at(0)) win();
			else if(opponentDice.at(0) > playerDice.at(0)) lose();
			else tie();
		}
		else if(opponentRoll == Roll::pair) {
			auto oppponentSingle = singleton(opponentDice);
			auto playerSingle = singleton(playerDice);

			if(playerSingle > oppponentSingle) win();
			else if(playerSingle < oppponentSingle) lose();
			else tie();
		}
		else tie();
	}
	else {
		if(static_cast<int>(playerRoll) > static_cast<int>(opponentRoll)) win();
		else lose();
	}
}


int singleton(const std::vector<int>& dice) {
	return dice.at(0) == dice.at(1) ? dice.at(2) : dice.at(0);
}

void win() {
	playerBalance += 2 * bet;
	std::cout << "You win!" << std::endl;
}

void lose() {
	opponentBalance += 2 * bet;
	std::cout << "You lose!" << std::endl;
}

void tie() {
	playerBalance += bet;
	opponentBalance += bet;
	std::cout << "You tied." << std::endl;
}