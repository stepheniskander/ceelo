#include <cstdlib>
#include <iostream>
#include "term.h"

void term::clear() {
	#ifdef _WIN32
		::system("cls");
	#else
		::system("clear");
	#endif
}

void term::pause() {
	std::cout << "Press <enter> to continue...";
	std::cin.ignore().get();
}