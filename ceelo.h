#pragma once

enum class Roll {lose, none, pair, trip, win};

//prompt bet until valid bet is entered
void prompt_bet();

// uses urng to set each die in vector to a value between 1 and 6
void roll_dice(std::vector<int>&, std::mt19937&);

// prints each die in vector
void print_dice(const std::vector<int>&);

// returns type of roll
Roll roll_type(const std::vector<int>&);

void compare_rolls();

// returns singleton in roll; only works with 3 dice
int singleton(const std::vector<int>&);

void win();
void lose();
void tie();